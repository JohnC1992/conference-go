from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_picture_url(location):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": location}
    url = "https://api.pexels.com/v1/search"
    request = requests.get(url, headers=headers, params=params)
    response = request.json()
    return response["photos"][0]["src"]["original"]


def get_location_coordinates(city, state, country="US"):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city},{state},{country}", "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url=url, params=params).json()

    return {
        "lat": response[0]["lat"],
        "lon": response[0]["lon"],
    }


def get_weather(city, state, country="US"):
    coordinates = get_location_coordinates(city, state)
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": coordinates["lat"],
        "lon": coordinates["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial" if country == "US" else "metric",
    }
    response = requests.get(url=url, params=params).json()
    return {
        "temperature": response["main"]["temp"],
        "description": response["weather"][0]["description"],
    }
