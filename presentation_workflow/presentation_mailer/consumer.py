import json
import time
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def reject(ch, method, properties, body):
    data = json.loads(body)
    email = data["presenter_email"]
    name = data["presenter_name"]
    title = data["title"]
    send_mail(
        "Your presentation has been rejected",
        f"We're sorry, {name}, but your presentation {title} has been rejected",
        "admin@conferece.go",
        [email],
        fail_silently=False,
    )
    ch.basic_ack(delivery_tag=method.delivery_tag)


def approve(ch, method, properties, body):
    data = json.loads(body)
    email = data["presenter_email"]
    name = data["presenter_name"]
    title = data["title"]
    send_mail(
        "Your presentation has been accepted",
        f"We're sorry, {name}, but your presentation {title} has been accepted",
        "admin@conferece.go",
        [email],
        fail_silently=False,
    )
    ch.basic_ack(delivery_tag=method.delivery_tag)


while True:
    try:
        parameters = pika.ConnectionParameters("rabbitmq")
        connection = pika.BlockingConnection(parameters=parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals", durable=True)
        channel.queue_declare(queue="presentation_rejections", durable=True)

        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=approve,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=reject,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("could not connect to RabbitMQ")
        time.sleep(2)
